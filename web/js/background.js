function Background(image) {
    ge.addBgEntity(this);
    
    this.lastDir = 0;
    this.posX = -500;
    this.posY = -200;
    this.ang = 0;
    this.nyX = 0;
    this.nyY = window.innerHeight/2 - 223/2;
    
    this.img = new Image(1920, 960);
    this.img.src = image;
}

Background.prototype = Object.create(Entity.prototype);

Background.prototype.constructor = Background;

Background.prototype.repaint = function(delta, ctx) {
    var r = Math.floor((Math.random() * 9) + 1); 
    if(r < 4)
        this.lastDir += 0
    else if(r < 6)
        this.lastDir+1;
    else if(r < 8)
        this.lastDir+=3;
    else
        this.lastDir+=2;

    this.lastDir %= 4;

    switch(this.lastDir) {
        case 0:
            this.posX++; this.ang+=0.002;
        break;
        case 1:
            this.posY++; this.ang+=0.001;
            break;
        case 2:
            this.posX--; this.ang-=0.001;
            break;
        case 3:
            this.posY--; this.ang-=0.002;
            break;
    }


    ctx.rotate(this.ang);
    ctx.drawImage(this.img, this.posX, this.posY);
    ctx.rotate(-this.ang);
};














function BackgroundAuto() {
    ge.addBgEntity(this);
    
    this.image = new Image();
    this.image.src = "http://jamestombs.co.uk/wp-content/uploads/2010/01/cloud_result.jpg";
    
    this.arr = [];
}

BackgroundAuto.prototype = Object.create(Background.prototype);
BackgroundAuto.prototype.constructor = BackgroundAuto;
BackgroundAuto.prototype.repaint = function(delta, ctx) {
    ctx.rect(0, 0, width, height);
    ctx.fillStyle="#B7E0FF";
    ctx.fill();
    
    for(var i = 0; i < this.arr.length; i++) {
        this.arr[i][0] += (this.arr[i][0] += (delta/500));
        
        if(this.arr[i][0] >= height)
            this.arr = this.arr.splice(i, 1);
        
        ctx.drawImage(this.image, this.arr[i][0], this.arr[i][1]);
    }
    
    this.arr.push([0, Math.floor(Math.random() * 1920)]);
};