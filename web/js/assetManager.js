function assetManager() {
    this.toLoad = [];
    this.loaded = [];
    
    this.loadCallback = [];
    
    this.currentLoadingGroupe = 0;
}

assetManager.prototype.IMAGE     = 1;
assetManager.prototype.SON       = 2;
assetManager.prototype.JS        = 3;
assetManager.prototype.LOCAL_URL = 4;
    
assetManager.prototype.addResource = function(data) {
    var id, url, groupeId, type, notRequired;
    
    if(typeof data == "string") url = data;
    else {
        id = data['id'];
        url = data['url'];
        groupeId = data['groupeId'];
        type = data['type'];
        notRequired = (data['notRequired'] === undefined) ? false : true;
    }
    
    if(groupeId === undefined) groupeId = -1;
    if(type === undefined) type = this.LOCAL_URL;
    
    
    if(this.toLoad[groupeId] === undefined) this.toLoad[groupeId] = [];
    
    
    this.toLoad[groupeId].push({
        'id': id,
        'url': url,
        'type': type,
        'object': null,
        notRequired: notRequired,
        failCount: 0
    });
}

assetManager.prototype.onLoad = function(grpId, f) { this.loadCallback[grpId] = f; }
assetManager.prototype.onAllLoaded = function(f) { this.onLoad(-1, f); }

assetManager.prototype.loadNext = function() {
    if(this.toLoad[this.currentLoadingGroupe] === undefined)
        return this.loadNextGroup();
    
    var item = this.toLoad[this.currentLoadingGroupe].shift();
    if(item !== undefined) this.loaded.push(item);
    
    if(item === undefined) return this.loadNextGroup();
    
    this.loadObj(item);
}

assetManager.prototype.loadObj = function(item) {
    var self = this;
    
    switch(item['type']) {//URL.createObjectURL
        case this.IMAGE:
            item.object = new Image();
            
            item.object.onload = function() {if(!item.notRequired) self.loadNext();}
            item.object.onerror = function(e) {
                if(++item.failCount <= 3)
                    self.loadObj(item);
                else {
                    console.warn("Surrend downloading " + item.url + " after " + item.failCount + "attempts");
                    if(!item.notRequired) self.loadNext();
                }
            }
            
            item.object.src = item.url;
            break;
        case this.JS:
            item.object = document.createElement("script");
            item.object.setAttribute("type", "text/javascript");
            item.object.setAttribute("src", item.url);
            
            item.object.onload = function() {if(!item.notRequired) self.loadNext();}
            
            document.body.appendChild(item.object);
            
            break;
        case this.SON:
        case this.LOCAL_URL:
            var xhr = new XMLHttpRequest();
            xhr.open("GET", item.url, true);
            xhr.responseType = "blob";
            xhr.timeout = 5000;
            
            xhr.onload = function (e) {
                if(item.type == self.SON) {
                    item.object = document.createElement("audio");
            
                    item.object.setAttribute("src", URL.createObjectURL(xhr.response));
                
                    document.body.appendChild(item.object);
                } else
                    item.object = URL.createObjectURL(xhr.response);
                                       
                if(!item.notRequired) self.loadNext();
            }

            xhr.send();
            break;
    }
    
    if(item.notRequired) this.loadNext();
}

assetManager.prototype.loadNextGroup = function() {
    console.log("[assetManager] Loaded group : " + this.currentLoadingGroupe);
    
    var f = this.loadCallback[this.currentLoadingGroupe];
    if(f !== undefined) f();
    
    
    if(this.currentLoadingGroupe == -1) return;
    
    this.currentLoadingGroupe++;
    
    if(this.toLoad[this.currentLoadingGroupe] === undefined) {
        this.currentLoadingGroupe = -1;
    }
    
    this.loadNext();
}


assetManager.prototype.loadGroup = function(grpId, f) {
    f = f | function() {};
}

assetManager.prototype.getIdByURL = function(str) {
    for(var i = 0; i < this.loaded.length; i++) {
        if(this.loaded[i].url == str || this.loaded[i].id == str)
            return i;
    }
    
    return -1;
}

assetManager.prototype.getResource = function(id) {
    if(typeof id === "string")
        id = this.getIdByURL(id);
    
    return this.loaded[id].object;
}