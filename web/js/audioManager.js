function audioManager() {
    var th = this;
    this.autoReplay = true;
    this.playlist = [];
    this.intervalId = -1;

    var btn = this.btn = document.getElementById("mainSvg");
    var playPause = this.playPause = document.getElementById("playPause");

    this.btn.addEventListener("click", function () {
//        if(th.elem.volume != 0) return;
        
        if (th.elem.paused) {
            th.fadeIn();
            
            document.getElementById("animReverse").setAttribute("begin", "mainSvg.click");
            document.getElementById("anim").setAttribute("begin", "indefinite");
        } else {
            th.fadeOut();
            
            document.getElementById("anim").setAttribute("begin", "mainSvg.click");
            document.getElementById("animReverse").setAttribute("begin", "indefinite");
        }
    }, false);
    
    this.btn.addEventListener("contextmenu", function(e){
        th.playNext();
        
        e.preventDefault();
        return false;
    }, false);
}

audioManager.prototype.fadeIn = function () {
    var elem = this.elem;
    clearInterval(this.intervalId);
    
    elem.play();
    var intervalId = this.intervalId = setInterval(function() {
        elem.volume += 0.1;
        
        if(elem.volume >= 0.9) {
            elem.volume = 1;
            clearInterval(intervalId);
        }
    }, 100);
};

audioManager.prototype.fadeOut = function () {
    var elem = this.elem;
    clearInterval(this.intervalId);
    
    var intervalId = this.intervalId = setInterval(function() {
        elem.volume -= 0.1;
        
        if(elem.volume <= 0.1) {
            elem.volume = 0;
            elem.pause();
            clearInterval(intervalId);
        }
    }, 100);
};


audioManager.prototype.play = function(url, type) {
    var th = this;
    
    if(this.elem === undefined) {
        this.elem = document.createElement("audio");
        document.body.appendChild(this.elem);
        this.elem.addEventListener("ended", function() {
            th.playNext();
        }, false);
    }
        
        
    this.elem.setAttribute("src", url);
    this.elem.setAttribute("type", type);
    this.elem.play();
    this.elem.currentTime = 0;
    
//    this.loadNext();
};

audioManager.prototype.addMusique = function (url) {
    this.playlist.push({
        url: "./son/" + url,
        lUrl: null,
        type: null
    });
};

audioManager.prototype.playNext = function () {
    if(this.playlist.length == 0) return;
    
    var th = this,
        autoReplay = this.autoReplay;

    if(this.playlist[0].lUrl === null)
        this.loadNext(function() { th.playNext(); });
    else {
        var obj = this.playlist.shift();
        if(autoReplay)
            this.playlist.push(obj);

        this.play(obj.lUrl, obj.type);
    }
};


audioManager.prototype.loadNext = function (loadedFunc) {
    if(loadedFunc === undefined) loadedFunc = function() {};
    
    var th = this,
        obj = this.playlist[0];

    if(obj.lUrl === null) {
        var th = this,
            xhr = window.xhr = new XMLHttpRequest();
        
        xhr.responseType = "blob";

        xhr.onload = function() {
            obj.lUrl = window.URL.createObjectURL(xhr.response);
            obj.type = xhr.response.type;

            loadedFunc();
        };

        xhr.open("GET", obj.url, true);
        xhr.send();
    } else
        loadedFunc();
};

audioManager.prototype.randomize = function () {
    var playlist2 = [];
    
    while(this.playlist.length != 0) {
        playlist2.push(this.playlist.splice(Math.floor(Math.random() * this.playlist.length), 1)[0])
    }
    
    this.playlist = playlist2;
};
