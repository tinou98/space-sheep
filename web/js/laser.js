function Laser(x, y, angle, parent) {
    VelocityEntity.call(this, x, y, angle, laserSpeed, aMngr.getResource("laser"));
    
    aMngr.getResource("laserfire01.ogg").currentTime = 0;
    aMngr.getResource("laserfire01.ogg").play();
    
    this.parent = parent;
    
    this.lifeTime = true;
}

Laser.prototype = Object.create(VelocityEntity.prototype);

Laser.prototype.repaint = function(delta, ctx) {
    //Repaint
    VelocityEntity.prototype.repaint.call(this, delta, ctx);
    
    // Teleport to screen border
    if(this.x > width || this.x < 0 || this.y > height || this.y < 0) {
        if(!this.lifeTime)
            this.delete();
            
        this.lifeTime = false;
        
        if(this.x > width) this.x = 0;
        if(this.x < 0) this.x = width;
        
        if(this.y > height) this.y = 0;
        if(this.y < 0) this.y = height;
    }
    
    // Collide test
    ge.fgElement.forEach(function(el) {
       if(el != this && el instanceof VelocityEntity && el.isInside(this.x, this.y)) {
           console.warn("isInsid !");
           if(el instanceof Sheep) {
               this.delete();
               el.kill();
           } else if(el instanceof Laser) {
               this.delete();
               el.delete();
           }
           
       }
    }, this);
};

Laser.prototype.delete = function() {
    this.parent.laserCounter--;
    
    VelocityEntity.prototype.delete.call(this);
};
