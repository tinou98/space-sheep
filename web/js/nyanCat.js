function NyanCat(x, y, angle) {
    VelocityEntity.call(this, x, y, angle, 2500, aMngr.getResource("nyancat"), 0, 0);
}

NyanCat.prototype = Object.create(VelocityEntity.prototype);

NyanCat.prototype.constructor = NyanCat;

NyanCat.prototype.repaint = function(delta, ctx) {
    // Repaint
    VelocityEntity.prototype.repaint.call(this, delta, ctx);
    
    
    
    // Collide test
    ge.fgElement.forEach(function(el) {
       if(el != this && el instanceof Sheep && el.isInside(this.x, this.y)) {
           console.warn("Nyan Kill !");
           el.kill();
       }
    }, this);
    
    
    // Avoid exit
    var max = (
        (this.image.width > this.image.height) ? this.image.width : this.image.height
    );
    
    
    if(this.x > width + max ||
       this.x < -max ||
       this.y > height + max ||
       this.y < -max)
        this.delete();

};