function VelocityEntity(x, y, angle, velocity, image, slowSpeed, slowAngle) {
    Entity.call(this);
    
    this.x = x;
    this.y = y;
    this.targetAngle = this.angle = angle;
    this.velocity = velocity;
    
    this.slowSpeed = slowSpeed | 0;
    this.slowAngle = slowAngle | 0;
    
    this.image = image;
    
    
    this.currG = 0;
}

VelocityEntity.prototype = Object.create(Entity.prototype);

VelocityEntity.prototype.constructor = VelocityEntity;

function max(a, b) {
    return (a >= b) ? a : b;
}

VelocityEntity.prototype.repaint = function(delta, ctx) {
    this.velocity -= this.velocity * this.slowSpeed * (delta / 1000);
    
    
    if(this.angle > Math.PI && this.targetAngle >  Math.PI) {
        this.angle -= 2*Math.PI;
        this.targetAngle -= 2*Math.PI;
    }
    if(this.angle < -Math.PI && this.targetAngle < -Math.PI) {
        this.angle += 2*Math.PI;
        this.targetAngle += 2*Math.PI;
    }
    
    this.angle += (this.targetAngle - this.angle) * this.slowAngle * (delta / 1000);
    
    this.x += (delta/1000) * this.velocity * Math.cos(this.angle);
    this.y += (delta/1000) * (this.velocity * Math.sin(this.angle) + this.currG);this.currG += G;

    if(this.y > height - 5) this.currG *= -0.25;

    ctx.save();
    
    ctx.translate(this.x, this.y);
    ctx.rotate(this.angle);

    ctx.drawImage(this.image, -this.image.width/2, -this.image.height/2);
    
    ctx.restore();
    
    if(this.name !== undefined) {
        ctx.font = "15px sf_archery";
        ctx.fillStyle = (this.font || "black");
        ctx.textAlign = "center";
        ctx.fillText(this.name, this.x, this.y - max(Math.abs(this.image.height*Math.cos(this.angle)), Math.abs(this.image.width*Math.sin(this.angle)))/2 - 10);
    }
    
    
    /*
    var x = this.x,
        y = this.y,
        dx = this.image.width/2,
        dy = this.image.height/2;    

    ctx.beginPath();
    ctx.moveTo(
        x + dx*Math.cos(this.angle) - dy*Math.sin(this.angle),
        y + dx*Math.sin(this.angle) + dy*Math.cos(this.angle)
    );
    ctx.lineTo(
        x + dx*Math.cos(this.angle) + dy*Math.sin(this.angle),
        y + dx*Math.sin(this.angle) - dy*Math.cos(this.angle)
    );
    ctx.lineTo(
        x - dx*Math.cos(this.angle) + dy*Math.sin(this.angle),
        y - dx*Math.sin(this.angle) - dy*Math.cos(this.angle)
    );
    ctx.lineTo(
        x - dx*Math.cos(this.angle) - dy*Math.sin(this.angle),
        y - dx*Math.sin(this.angle) + dy*Math.cos(this.angle)
    );
    ctx.closePath();
    ctx.stroke();
    
    ctx.beginPath();
    ctx.arc(this.x, this.y, ((this.image.width > this.image.height) ? this.image.width : this.image.height) / 2, 0, 2*Math.PI);
    ctx.stroke();*/
};

VelocityEntity.prototype.isInside = function(xT, yT) {
    if((Math.pow(xT - this.x, 2) + Math.pow(yT - this.y, 2)) > Math.pow(((this.image.width > this.image.height) ? this.image.width : this.image.height) / 2, 2)) {
        return false;
    }  
    
    var a = Math.sin(this.angle),
        b = -Math.cos(this.angle),
        c = - a*this.x - b*this.y;
    
    if(Math.abs(a*xT + b*yT + c) > this.image.height/2) {
        return false;
    }
    
    return true;
};