function Sheep(x, y, sheepImg, hearthImg, hearthSide, name) {
    VelocityEntity.call(this, x, y, 0, 0, sheepImg, speedVar, angleVar);
    
    this.lastDelay = 0;
    this.laserCounter = 0;
    
    this.lifeCount = lifeCount; // <- load from const
    
    this.hearth = hearthImg;
    
    this.name = name;
    
    switch(hearthSide) {
        default:
        case "left":
        case "l":
            this.hearthSide = 'l';
            break;
        case "right":
        case "r":
            this.hearthSide = 'r';
            break;
    }
    
    this.unKillable = 0;
}

Sheep.prototype = Object.create(VelocityEntity.prototype);

Sheep.prototype.constructor = Sheep;

Sheep.prototype.isKillable = function() { return this.unKillable <= 0; }

Sheep.prototype.spawn = function() {
    this.unKillable = safeSpawnTime;
        
    console.log("I'm " + ge.localPseudo + "-" + ge.distPseudo);
    
    if(ge.localEntity == this) { // On calcule pour l'entité locale
        this.x = (ge.localPseudo.localeCompare(ge.distPseudo) == 1) ? width - 25 : 25;
    } else { // On calcule pour l'entité distante
        this.x = (ge.distPseudo.localeCompare(ge.localPseudo) == 1) ? width - 25 : 25;
    }
    
    this.y = height/2;
}

Sheep.prototype.repaint = function(delta, ctx) {
    if(this.lifeCount <= 0) return;
    
    this.unKillable -= delta;
    
    this.lastDelay += delta;
    
    // Compute target angle
    this.targetAngle = this.angle;
    
    // If not too close
    if(Math.pow(this.x - this.targetX, 2) + Math.pow(this.y - this.targetY, 2) > 25)
        this.targetAngle = Math.atan2(this.targetY - this.y, this.targetX - this.x);
    
    
    // Avoid jump on limit ...
    if(this.targetAngle - this.angle > Math.PI)
        this.targetAngle -= 2*Math.PI;
    
    if(this.targetAngle - this.angle < -Math.PI)
        this.targetAngle += 2*Math.PI;
    
    
    // Repaint
    VelocityEntity.prototype.repaint.call(this, delta, ctx);
    
    
    // If mouse down keep min speed
    if(this.mouseDown && this.velocity < moutonMinSpeed)
        this.velocity = moutonMinSpeed;
    
    
    // Avoid exit
    if(this.x > width) this.x = width;
    if(this.x < 0) this.x = 0;
    if(this.y > height) this.y = height;
    if(this.y < 0) this.y = 0;
    
    if(!this.isKillable()) {
        full = 2/3 * max(this.image.height, this.image.width);
        for(var r = full; r > 0; r--) {
            var val = Math.pow(r/full, 25) * ((this.unKillable > 500) ? 1 : this.unKillable * .002),
                green = parseInt((this.unKillable > 1000) ? 255 : this.unKillable * 0.255),
                red = 255 - green;
            ctx.strokeStyle = "rgba(" + red + ", " + green + ", 0, " + ((val < 0) ? 0 : val) + ")";
            ctx.beginPath();
            ctx.arc(this.x, this.y, r, 0, 2*Math.PI);
            ctx.stroke();
        }
    }
    
    ctx.font="30px Arial";
    
    if(this.hearthSide == 'l') {
        ctx.textAlign = "start";
        ctx.fillText("Votre vie :", 0, height - 30 - this.hearth.height)
    } else {
        ctx.textAlign = "end";
        ctx.fillText("Sa vie :", width, height - 30 - this.hearth.height)
    }
    
    ctx.textAlign = "";
    
    for(var i = 0; i < this.lifeCount; i++) {
        if(this.hearthSide == 'l')
            ctx.drawImage(this.hearth, 30 * i, height - 30);
        else
            ctx.drawImage(this.hearth, width - 30 * (i+1) , height - 30);
    }
    
};

Sheep.prototype.move = function() {this.currG = 0;
    this.velocity += moutonAddSpeed;
    
    if(this.velocity >= moutonMaxSpeed)
        this.velocity = moutonMaxSpeed;
};

Sheep.prototype.shot = function() {
    if(this.laserCounter < maxLaser && this.lastDelay > minDelay) {
        if(this == ge.localEntity)
            window.rtc.send("#" + this.serialize() + ";1")
        
        this.lastDelay = 0;
        this.velocity -= moutonReculeShot;
        this.laserCounter++;
        
        new Laser(
this.x + (this.image.width*Math.cos(this.angle)),
this.y + (this.image.width*Math.sin(this.angle)),
            this.angle, this);
    }
};

Sheep.prototype.kill = function() {
    if(!this.isKillable()) return;
    
    if(this != ge.localEntity) return window.rtc.send("die");
    
    this.lifeCount--;

    if(this.lifeCount <= 0) {
        alert("You died !")
    }
    
    this.spawn();
};

Sheep.prototype.serialize = function() {
    return this.x + ";" + this.y + ";" + this.angle + ";" + this.velocity + ";" + this.laserCounter + ";" + this.lifeCount + ";" + this.unKillable
}

Sheep.prototype.parse = function(str) {
    str = str.split(";");
    
    this.x = parseFloat(str[0]);
    this.y = parseFloat(str[1]);
    //this.angle = parseInt(str[2]);
    this.velocity = parseFloat(str[3]);
    this.laserCounter = parseInt(str[4]);
    this.lifeCount = parseInt(str[5]);
    this.unKillable = parseInt(str[6]);

    if(str[7] == 1) {
        this.angle = parseFloat(str[2]);
        this.shot();
    }
}
