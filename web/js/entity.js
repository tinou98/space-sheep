function Entity() {
    ge.addFgEntity(this);
}

Entity.prototype.repaint = function() {
    console.warn("Sub class of Entity should re-implement repaint");
};

Entity.prototype.delete = function() {
    ge.deleteEntity(this);
};
