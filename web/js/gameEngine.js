window.ge = {
    fgElement: [],
    bgElement: [],

    prev: null,
    cumul: 0,

    bg: document.getElementById("bg"),
    fg: document.getElementById("fg"),
    bgCtx: bg.getContext("2d"),
    fgCtx: fg.getContext("2d"),

    finished: false,

    repaintLoop: function(e) {
        var delta = 0; 
        if(ge.prev != null) delta = e - ge.prev;
        else ge.cumul = 9999;

        ge.cumul += delta;

        ge.fgCtx.clearRect(0, 0, width, height);

        ge.fgElement.forEach(function(e) {
            try {
                e.repaint(delta, ge.fgCtx);
            } catch(err) {
                console.warn("Kill ", e, " cause send the error : ", err);
                e.delete();
            };
        });



        if(ge.cumul >= 1000) {
            ge.bgCtx.clearRect(0, 0, width, height);

            ge.bgElement.forEach(function(e) {
                try{
                    e.repaint(ge.cumul, ge.bgCtx);
                } catch(err) {
                    console.warn("Kill ", e, " cause send the error : ", err);
                    e.delete();
                };
            });

            ge.cumul = 0;
        }

        ge.prev = e;

        if(!ge.finished)
            window.requestAnimationFrame(ge.repaintLoop);
    },

    addFgEntity: function(e) {
        this.fgElement.push(e);
    },

    addBgEntity: function(e) {
        this.bgElement.push(e);
    },

    deleteEntity: function(e) {
        var id = this.fgElement.indexOf(e);
        if(id != -1) this.fgElement.splice(id, 1);

        id = this.bgElement.indexOf(e);
        if(id != -1) this.bgElement.splice(id, 1);
    },

    localEntity: null,
    distEntity: null,
    
    localPseudo: null,
    distPseudo: null,
}

bg.width =  fg.width =  width;
bg.height = fg.height = height;

bg.style.width = fg.style.width = width + "px";
bg.style.height = fg.style.height = height + "px";