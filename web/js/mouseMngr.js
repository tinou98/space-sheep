function MouseMngr(arr, object, rtc, distObject) {
    var object = this.object = object;
    var distObject = this.distObject = distObject;
    this.arr = arr;
    this.rtc = rtc;

    selfMouseMngr = this;
    
    window.addEventListener("keypress", function(e) {
        if(e.charCode == 103) {
            G -= 20;
            
            if(G < -20) G = 20;
            
            selfMouseMngr.sendEvent("G;" + G);
        }
        
        selfMouseMngr.call(object, selfMouseMngr.arr["keyboard"][e.charCode]);
    });
    
    // Set mouse pos on change
    object.targetX = object.targetY = 0;
    document.addEventListener("mousemove", function(e) {
//        object.mouseDown = (e.buttons & 0b010);
        
        object.targetX = (e.pageX - window.dLeft)/ratio;
        object.targetY = (e.pageY - window.dTop)/ratio;
        
        selfMouseMngr.sendEvent("mousemove;" + object.targetX + ";" + object.targetY);
    }, true);

    // Reset mouseDown val
    document.addEventListener("mouseup", function(e) {
        if(!e.buttons & 0b001 || e.button == 0) {
            selfMouseMngr.sendEvent("mouseup;");
        
            object.mouseDown = false;
        }
    }, true);
    
    // Set mouseDown val & call func
    document.addEventListener("mousedown", function(e) {
        if(e.buttons & 0b001) {
            object.mouseDown = true;
            
            selfMouseMngr.sendEvent("mousedown;" + e.buttons);
        }
        
        selfMouseMngr.call(object, selfMouseMngr.arr["mouse"][e.button]);
    }, true);
    
    // Block opening Right Click
    document.addEventListener("contextmenu", function(e) {
        e.preventDefault();
        return false;
    }, true);
}

MouseMngr.prototype.call = function(object, fct, param) {
    if(typeof object[fct] == "function")
        return object[fct].call(object, param);
    
    return null;
};

MouseMngr.prototype.sendEvent = function(str) {
    if(window.rtc !== undefined && (rtc.dataChannel.readyState === "open" || rtc.dataChannel.fallBack)) {
        this.rtc.send("NE" + str);
    }
};


MouseMngr.prototype.netEvent = function(str) {
    str = str.split(";");
    switch(str[0]) {
        case "mousemove":
            this.distObject.targetX = str[1];
            this.distObject.targetY = str[2];
            break;
        case "mouseup":
            this.distObject.mouseDown = false;
            break;
        case "mousedown":
            this.distObject.mouseDown = true;
            break;
        case "G":
            G = str[1];
            break;
    }
};