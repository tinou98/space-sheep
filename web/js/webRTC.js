function setupFallback() {
    if(rtc.dataChannel && rtc.dataChannel.fallBack) {
        if(!rtc.dataChannel.connected) {
            console.log("onopen handler");
            rtc.dataChannel.connected = true;
            rtc.dataChannel.onopen();
        }
        
        return;
    }
    
    console.log("setupFallback");
    
    window.rtc.dataChannel = {fallBack: true, connected: false};
    window.rtc.setupDataChannel(rtc.dataChannel);
                
    window.rtc.send = function(msg) {
        window.socket.emit("rtc", msg);
    }
    
    window.rtc.send("setupFallback");
    
    setupFallback();
}

function WebRTC(signal, opt) {
    var self = this;
    this.localConnection = new RTCPeerConnection(opt, null);
    this.localConnection.onsignalingstatechange = function() {
        if(self.localConnection.signalingState != "have-local-offer")
            while(self.ICECandidate.length !== 0)
                self.localConnection.addIceCandidate(new RTCIceCandidate(self.ICECandidate.pop()));
    };
    this.localConnection.onicecandidate = function iceCallback(event) {
        if (event.candidate) {
            self.signalSend("I" + JSON.stringify(event.candidate));
        }
    };
    this.localConnection.oniceconnectionstatechange = function() {
        if (self.localConnection.iceConnectionState == "failed") {
            cdt = "";
            for(var  i = 0; i < window.ICECandidate.length; i++)
                cdt += "<hr>" + window.ICECandidate[i].candidate;
                
            alert("Impossible de trouver d'établir une connexion<br>Utilisation de la solution de fallback", "Ok", setupFallback);
        }
    };
    this.localConnection.ondatachannel = function (e) {
        console.log("Chanel");
        self.dataChannel = e.channel;
        
        self.setupDataChannel(self.dataChannel);
    };
    
    
    this.dataChannel = null;
    
    this.ICECandidate = [];
    window.ICECandidate = [];
    this.signalSend = signal;
    
    
    this._onopen = function() {};
    this._onclose = function() {};
    this._onerror = function() {};
    this._onmessage = function() {};
    
    
    this.lastPingSend = 0;
    this.moyPing = new MoyUtils("ms");
    window.tmp = new showMoy(this.moyPing, 1920, 20, "end");
}


function logError(err) { console.log(err); };

WebRTC.prototype.setupDataChannel = function(dataChannel) {
    var self = this;
    
    dataChannel.onopen = function() {
        console.log("Data Channel OPEN");
        setInterval(function(){self.ping();}, 5000);
        self._onopen.call(self);
    };
    dataChannel.onclose = function() { console.log("Data Channel CLOSED"); self._onclose.call(self); };
    dataChannel.onerror = function(e) { console.log("Send channel error : ", e); self._onerror.call(self, e); };
    dataChannel.onmessage = function(e) {
//        console.log("Received : ", e);
        if(e.data == "setupFallback")
            setupFallback();
        
        if(e.data.startsWith("PING"))
            window.rtc.send("PONG");
        
        else if(e.data.startsWith("PONG")) {
            var ping = (new Date().getTime()) - self.lastPingSend;
            self.moyPing.addValue(ping);
        } else
            self._onmessage.call(self, e)
    };
};

WebRTC.prototype.ping = function() {
    if(this.dataChannel === null)
        return;
    
    this.send("PING");
    this.lastPingSend = new Date().getTime();
}

WebRTC.prototype.gotDescription = function(desc) {
    this.localConnection.setLocalDescription(desc);
    
    this.signalSend("D" + JSON.stringify(desc));
}

// On text received from signaling
WebRTC.prototype.receiveText = function(msg) {
    var self = this;
    if(msg[0] == "D") {
        this.localConnection.setRemoteDescription(
            new RTCSessionDescription(JSON.parse(msg.substring(1))),
            function() {
                if(self.localConnection.localDescription === null ||
                   self.localConnection.localDescription.sdp === '') {
                    self.localConnection.createAnswer(function(desc) {self.gotDescription(desc);}, logError);
                }
            },
            logError);
    } else if(msg[0] == "I") {
        this.ICECandidate.push(JSON.parse(msg.substring(1)));
        window.ICECandidate.push(JSON.parse(msg.substring(1)));
        
        this.localConnection.onsignalingstatechange();
    } else {
        console.warn("RTC ?", str);
    }
}

WebRTC.prototype.startHandShake = function(msg) {
    self = this;
    
    this.dataChannel = this.localConnection.createDataChannel('game');
    self.setupDataChannel(this.dataChannel);
    
    this.localConnection.createOffer(function(desc) {self.gotDescription(desc);}, logError);
}


WebRTC.prototype.send = function(msg) {
    this.dataChannel.send(msg);
}

WebRTC.prototype.setOnOpen = function(f) { this._onopen = f; }
WebRTC.prototype.setOnClose = function(f) { this._onclose = f; }
WebRTC.prototype.setOnError = function(f) { this._onerror = f; }
WebRTC.prototype.setOnMessage = function(f) { this._onmessage = f; }
