function MoyUtils(unit) {
    this.sum = 0;
    this.numVal = 0;
    
    this.min = Number.POSITIVE_INFINITY;
    this.max = Number.NEGATIVE_INFINITY;
    
    this.lastVal = NaN;
    this.unit = unit;
}

MoyUtils.prototype.addValue = function(val) {
    this.sum += val;
    this.numVal++;
    
    if(val < this.min) this.min = val;
    if(val > this.max) this.max = val;
    
    this.lastVal = val;
}

MoyUtils.prototype.moy = function() {
    return this.sum/this.numVal;
}

MoyUtils.prototype.toString = function(prec) {
    prec = prec | 2;
    return [
        this.lastVal.toFixed(prec) + " " + this.unit,
        this.min.toFixed(prec) + "-" + this.max.toFixed(prec) + " : " + this.moy().toFixed(prec)
    ]
}



function fpsUtils() {
    Entity.call(this);
    
    this.fps = new MoyUtils();
    this.fpsLastVal = 0;
    this.fpsDelayer = 0;
}

fpsUtils.prototype = Object.create(Entity.prototype);
fpsUtils.prototype.constructor = fpsUtils;

fpsUtils.prototype.repaint = function(delta, ctx) {
    if(delta != 0) 
        this.fps.addValue(1000/delta);
        
    this.fpsDelayer = (++this.fpsDelayer) % 51;
    if(this.fpsDelayer == 50)
        this.fpsLastVal = (1000/delta).toFixed(2);
    
    ctx.fillStyle = "red";
    ctx.font = "bold 20px sf_archery";
    
    ctx.textAlign = "start";
    
    ctx.fillText(this.fpsLastVal + " fps", 2, 20);
    ctx.fillText(this.fps.toString()[1], 2, 40);
};


function showMoy(mu, x, y, align) {
    Entity.call(this);
    
    this.x = x;
    this.y = y;
    this.align = align || "start";
    
    this.moyUtils = mu;
}

showMoy.prototype = Object.create(Entity.prototype);
showMoy.prototype.constructor = showMoy;

showMoy.prototype.repaint = function(delta, ctx) {
    ctx.font = "bold 20px sf_archery";
    ctx.textAlign = this.align;
    
    ctx.fillStyle = (this.moyUtils.lastVal < 150) ? "green" : "red";
    ctx.fillText(this.moyUtils.toString()[0], this.x, this.y);
    
    ctx.fillStyle = (this.moyUtils.moy() < 150) ? "green" : "red";
    ctx.fillText(this.moyUtils.toString()[1], this.x, this.y + 20);
};