window.aMngr = new assetManager();

/* Groupe : 0 login Page */ {
    //aMngr.addResource({groupeId:0, type: aMngr.SON, url:"8bitLife.mp3", notRequired: true});

    aMngr.addResource({groupeId:0, type: aMngr.JS, url:"js/const.js"});
    aMngr.onLoad(0, function() {
        //aMngr.getResource("8bitLife.mp3").volume = 0.2;
        //aMngr.getResource("8bitLife.mp3").play();
    });
}
/* Groupe : 1 connect Step */ {
    aMngr.addResource({groupeId:1, type: aMngr.JS, url:"https://cdn.socket.io/socket.io-1.3.7.js"});
    aMngr.addResource({groupeId:1, type: aMngr.JS, url:"adapter.js"});
    aMngr.addResource({groupeId:1, type: aMngr.JS, url:"js/webRTC.js"});
    aMngr.onLoad(1, function() {
        // On crée la connection
        //window.socket = createSocket("http://ws.space-sheep.fr.nf:8000");
        window.socket = createSocket();
/*        setTimeout(function() {
            if(!socket.connected) {
                console.warn("Fallback port 80");
                socket.disconnect();
                window.socket = createSocket("http://ws.space-sheep.fr.nf");
            }
        }, 10000);*/
    });
}
/* Groupe : 2 game main assets */ {
    aMngr.addResource({groupeId:2, type: aMngr.JS, url:"js/gameEngine.js"});
    aMngr.addResource({groupeId:2, type: aMngr.JS, url:"js/entity.js"});
    aMngr.addResource({groupeId:2, type: aMngr.JS, url:"js/velocityEntity.js"});
    aMngr.addResource({groupeId:2, type: aMngr.JS, url:"js/mouseMngr.js"});
    aMngr.addResource({groupeId:2, type: aMngr.JS, url:"js/background.js"});
    aMngr.addResource({groupeId:2, type: aMngr.JS, url:"js/fpsUtils.js"});
    aMngr.addResource({groupeId:2, type: aMngr.JS, url:"js/laser.js"});
    aMngr.addResource({groupeId:2, type: aMngr.JS, url:"js/sheep.js"});
    aMngr.addResource({groupeId:2, type: aMngr.JS, url:"js/nyanCat.js"});



    aMngr.addResource({groupeId:2, type: aMngr.IMAGE, url:"NyanCat.png", id: "nyancat"});


    aMngr.addResource({groupeId:2, type: aMngr.IMAGE, url:"sheep.png", id: "sheep"});
    aMngr.addResource({groupeId:2, type: aMngr.IMAGE, url:"hearth.png", id: "hearth"});
    aMngr.addResource({groupeId:2, type: aMngr.IMAGE, url:"laser2.png", id: "laser"});
    aMngr.addResource({groupeId:2, type: aMngr.SON, url:"laserfire01.ogg", id: "laserSound"});
    //    aMngr.addResource({groupeId:2, type: aMngr.IMAGE, url:"sky.png", id: "sky"});

    aMngr.onLoad(2, function() {
        document.getElementById("logo").className += "ed";
    });
}

aMngr.loadNext();

document.getElementById("pseudo").onkeypress = function(e) {
    if(e.keyCode == 13) {
        ge.localPseudo = document.getElementById("pseudo").value;
        socket.emit("setId", document.getElementById("pseudo").value);
    }
}

function createSocket(url) {
    var socket = io(url);

    // Answer login
    socket.on("setId", function(data) {
        if(data[0] == 'O') {
            document.getElementById("overlay").className = "login";

            // Fetch player list
            socket.emit("getList");
        }  else
            document.getElementById("pseudo").value = data.substring(1);
    });

    // Recieve item in list
    function addItem(data) {
        var c = document.createElement("div");
        c.innerHTML = data;
        c.addEventListener("click", function() {
            socket.emit("connTo", data);
            ge.distPseudo = data;
        });
        document.getElementById("pairList").appendChild(c);
    }
    socket.on("newListItem", addItem);

    // Recieve itemS in list
    socket.on("listItem", function(data) {
        document.getElementById("pairList").innerHTML = "";
        data.split(",").forEach(addItem);
    });

    // Pair disconnected
    socket.on("pairQuit", function(data) {
        var childs = document.getElementById("pairList").childNodes
        for(var i = 0; i < childs.length; i++) {
            if(childs[i].innerHTML = data) {
                childs[i].remove();
            }
        }
    });

    socket.on("askConn", function(data) {
        ge.distPseudo = data;
        confirm("<b>" + data + "</b> voudrais jouer avec toi",
                "Accepter", function() { socket.emit("askConn", "A");},
                "Refuser", function() { socket.emit("askConn", "D");}
               );
    });

    socket.on("connTo", function(data) {
        if(data[0] == 'F') {
            alert(data.substring(1));

            // Request pair list
            socket.emit("getList");
        } else if(data[0] == 'R') {
            alert("La paire a refusé la connexion", "Ok", function(){});
        } else {
            initRtc();
            rtc.startHandShake();
        }
    });

    socket.on("send", function(data) {
        // Si on recoit un message, et que rien n'écoute, on crée la paire locale
        if(!window.rtc) initRtc();

        rtc.receiveText(data);
    });

    socket.on("connected", function() {
        document.getElementById("logo").className = "load" + document.getElementById("logo").className;
    });

    socket.on("rtc", function(data) {
        if(!rtc.dataChannel)
            setupFallback();

        rtc.dataChannel.onmessage.call(rtc.dataChannel, {data: data});
    });
    
    return socket;
}

// Start game
function startGame() {
    document.getElementById("overlay").className += " done";
    // Delay during half-time for animation
    setTimeout(function() {
        // Start animation
        document.getElementById("sheep").className = "go";
    }, 1500);

    setTimeout(function() {
        // Deactivate login page
        document.getElementById("overlay").style.display = "none";
        document.getElementById("game").style.display = "";
    }, 2000);

    // On crée le fond d'écran
    //        new Background("sky.png");
    //new BackgroundAuto();//("sky.png");

    // On crée l'entité locale
    ge.localEntity = new Sheep(width * 0.25, height/2, aMngr.getResource("sheep"), aMngr.getResource("hearth"), "left", ge.localPseudo); ge.localEntity.font = "green";
    ge.distEntity = new Sheep(width * 0.75, height/2, aMngr.getResource("sheep"), aMngr.getResource("hearth"), "right", ge.distPseudo); ge.distEntity.font = "red";

    ge.localEntity.spawn(); ge.localEntity.unKillable = 6000;
    ge.distEntity.spawn(); ge.distEntity.unKillable = 6000;

    // On charge le gestionnaire de souris/clavier
    window.mouseMngr = new MouseMngr(shortcut, ge.localEntity, window.rtc, ge.distEntity);

    // On affiche les fps
    new fpsUtils();

    // On démare le rendu
    ge.repaintLoop();
}

function initRtc() {
    window.rtc = new WebRTC(
        function(msg) {socket.emit("send", msg);},
        RTCConfig
    );

    rtc.setOnMessage(function(msg) {
        /*if(msg.data == "connectDone") {
            return startGame();
        }*/

        if(msg.data[0] == "#") {
            ge.distEntity.parse(msg.data.substring(1))
        } else if(msg.data[0] == "N" && msg.data[1] == "E") {
            mouseMngr.netEvent(msg.data.substring(2));
        } else if(msg.data == "die") {
            ge.localEntity.kill();
        }
    });

    rtc.setOnOpen(function() {
        var fallBack = rtc.dataChannel.fallBack
        startGame();

        if(!fallBack) {
            console.log("Close socket");
            socket.close();
        }

        //window.rtc.dataChannel.send("connectDone");

        setInterval(function() {
            //console.warn("#" + ge.localEntity.serialize());
            rtc.send("#" + ge.localEntity.serialize());
        }, 100);
    });
}

function alert(msg, btnStr, btnF) { confirm(msg, btnStr, btnF, "", function(){}); }
function confirm(pseudo, btn1Str, btn1F, btn2Str, btn2F) {
    var btn1Str = (btn1Str != undefined) ? btn1Str : "Accepter";
    var btn2Str = (btn2Str != undefined) ? btn2Str : "Refuser";

    document.getElementById("connPseudo").innerHTML = pseudo;
    var overlay = document.getElementById("connOverlay");

    document.getElementById("accept").innerHTML = btn1Str;
    document.getElementById("accept").style.display = (btn1Str != "") ? "inline-block" : "none";

    document.getElementById("refused").innerHTML = btn2Str;
    document.getElementById("refused").style.display = (btn2Str != "") ? "inline-block" : "none";
    overlay.className = "show";

    document.getElementById("accept").onclick = function() {
        overlay.className = "";

        if(typeof btn1F === "function")
            btn1F();
    }
    document.getElementById("refused").onclick = function() {
        overlay.className = "";

        if(typeof btn2F === "function")
            btn2F();
    }
}

function min(a, b) {return (a < b) ? a : b;}

function resize() {
    window.ratio = Math.floor(
        min(window.innerWidth / 1920, window.innerHeight / 1080) * 1000
    ) / 1000;

    window.dTop = (window.innerHeight - 1080*ratio)/ 2;
    window.dLeft = (window.innerWidth - 1920*ratio)/ 2;
    document.getElementById("game").style.transform = "scale(" + ratio + ")";
    document.getElementById("game").style.transformOrigin = 
        dLeft / (1-ratio) + "px " +
        dTop  / (1-ratio) + "px";
}

window.onresize = resize;
resize();
