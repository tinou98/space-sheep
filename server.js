#!/bin/env node

require('colors');

function log(str, state) {
	if(state === undefined) state = "INFO".blue;

	var time = new Date();
	console.log(   (("0" + time.getHours()).slice(-2) + ":" + 
					("0" + time.getMinutes()).slice(-2) + ":" + 
					("0" + time.getSeconds()).slice(-2)).grey, "[" + state + "] " + str);
}

function logIO(msg) {log(msg, " IO ".cyan);}
function logOK(msg) {log(msg, " OK ".green);}
function logFAIL(msg) {log(msg, "FAIL".red);}
function logPENDING(msg) {log(msg, "....".grey);}
function logFAILMySQL(msg) {log(msg, "MySQL".red);}

logPENDING("Loading");

if (!String.prototype.endsWith) {
	String.prototype.endsWith = function(searchString, position) {
		var subjectString = this.toString();
		if(
			typeof position !== 'number' ||
			!isFinite(position) ||
			Math.floor(position) !== position ||
			position > subjectString.length
		) position = subjectString.length;

		position -= searchString.length;
		var lastIndex = subjectString.indexOf(searchString, position);
		return lastIndex !== -1 && lastIndex === position;
	};
}


var app,
	io,
	fs,
	url,
	mime,
	mysql;


try {
	app   = require('http').createServer(handler)
	io    = require('socket.io')(app);
	fs    = require('fs');
	url   = require("url");
	mime  = require('mime');
	mysql = require('mysql');
} catch(e) {
	logFAIL(e.code + " - " + e.message);
	process.exit();
}

logPENDING("Start MySQL connection");
var connection;
try {
	connection = mysql.createConnection({
		host     : process.env.OPENSHIFT_MYSQL_DB_HOST || 'localhost',
		port     : process.env.OPENSHIFT_MYSQL_DB_PORT || '3306',
		user     : process.env.OPENSHIFT_MYSQL_DB_USERNAME || 'adminCazDwKd',
		password : process.env.OPENSHIFT_MYSQL_DB_PASSWORD || 'K2Ablv_1Vlm_',
		database : process.env.OPENSHIFT_GEAR_NAME || 'data'
	});

	connection.connect(function(err) {
		if (err) {
			logFAIL("MySQL connection failed : " + err.message);
			connection = {
				query: function(q) {logFAILMySQL("Access denied (query " + q + ")")},
				end: function() {},
				ok: false
			}
		}
	});

	connection["ok"] = true;
} catch (e) {
	logFAIL("Start MySQL connection");
	connection = {
				query: function(q) {logFAILMySQL("Access denied (query " + q + ")")},
				end: function() {},
				ok: false
			}
}
if(connection["ok"])
	logOK("Start MySQL connection");

var listenPort = process.env.OPENSHIFT_NODEJS_PORT || process.env.OPENSHIFT_INTERNAL_PORT || process.env.PORT || 80,
     listenUrl = process.env.OPENSHIFT_NODEJS_IP || process.env.OPENSHIFT_INTERNAL_IP || process.env.IP || "0.0.0.0";
log("PORT " + listenPort + "\t\tURL " + listenUrl);
app.listen(listenPort, listenUrl);


function handler(req, res) {
	var path = url.parse(req.url).pathname;

	if(path == "/exit")
		return process.exit();


	if(path == "/") path = "/index.html";

    /* Reply to Google App Engine */
	if(path == "/_ah/health") {
        res.writeHead(200);
        res.end();
        return;
    }

	var canBeCached = new Date(req.headers["if-modified-since"]) > startTime;

	log(path, ("HTTP" + 
			(canBeCached ? "-CACHED".magenta : "") +
			(req.headers.hasOwnProperty('range') ? "-PARTIAL".cyan : "")).yellow
	);

	if(canBeCached) {
		res.writeHead(304);

		res.end();
	} else
		loadFile(__dirname + "/web/" + path, req, res);
}

function loadFile(path, req, res) {
	fs.stat(path, function(err, stat) {
		if (err) {
			res.writeHead(500, {'Content-Type': "text/html; charset=utf-8"});
			return res.end("<html><h1>The page " + path + " can't be loaded</h1><br>You may try come back <a href='http://space-sheep.fr.nf/'>here</a></html>");
		}

		var mimeType = mime.lookup(path);

		if(path.endsWith("html"))
			mimeType = "text/html; charset=utf-8";
		if(path.endsWith("js"))
			mimeType = "application/javascript; charset=utf-8";
		if(path.endsWith("css"))
			mimeType = "text/css; charset=utf-8";

		if(req.headers.hasOwnProperty('range')) {
			var range = /^bytes.*:.*(\d+)-(\d+)$/.exec(req.headers['range']);
			res.writeHead(206, {
				'Content-Range': "bytes: " + range[1] + "-" + range[2] + "/" + stat.size,
				'Content-Type': mimeType,
				'Content-Length': range[2] - range[1] + 1,
				'Last-Modified': (new Date()).toUTCString(),
				'Cache-Control': "max-age=864000"
			});

			fs.createReadStream(path, {
				start: Number(range[1]),
				end:   Number(range[2])
			}).pipe(res);
		} else {
			res.writeHead(200, {
				'Content-Type': mimeType,
				'Content-Length': stat.size,
				'Last-Modified': (new Date()).toUTCString(),
				'Cache-Control': "max-age=864000"
			});

			if(req.method == "HEAD")
				res.end();
			else
				fs.createReadStream(path).pipe(res);
		}
	});
}

// Pseudo -> [connected?, socket, otherSocket]
var pair = {};

function getMyPseudo(socket) {
	for (var key in pair) {
		if(pair[key][1] == socket)
			return key;
	}
}

io.on('connection', function (socket) {
	connection.query('CALL `add`();');

	socket.emit('connected');
	socket.on('disconnect', function(data) {
		var pseudo = "On sait pas ki ?";
		for (var key in pair) {
			if(pair[key][1] == this) {
				pseudo = key;
			}
		}
		logIO("[X] " + pseudo)

		delete pair[pseudo];

		for (var key in pair) {
			pair[key][1].emit("pairQuit", pseudo);
		}
	});

	socket.on('setId', function (data) {
		if(pair[data] == undefined) { // Ce nom n'existe pas : OK
			this.emit("setId", "O"); logIO("[O] " + data)
			pair[data] = [false, this, 0];
			for (var key in pair) {
				if(!pair[key][0] && pair[key][1] != this) {
					pair[key][1].emit("newListItem", data); // On envoit le pseudo aux autres
				}
			}
		} else {
			this.emit("setId", "FDéja utilisé");
		}
	});


	socket.on('getList', function () {
		var list = "";
		for (var key in pair) {
			if(!pair[key][0] && pair[key][1] != this)
				list += key + ",";
		}

		this.emit("listItem", list.substring(0, list.length - 1));
	});


	socket.on('connTo', function (data) {
		if(pair[data] == undefined)
			this.emit("connTo", "FPas de paire");
		else if(pair[data][0])
			this.emit("connTo", "FLa paire est déja connecté");
		else {
			var pseudo = getMyPseudo(this);
			
			pair[pseudo][0] = true;
			pair[pseudo][2] = pair[data][1];

			pair[data][0] = true;
			pair[data][2] = pair[pseudo][1];

			pair[pseudo][2].emit("askConn", pseudo);

			logIO("[?] " + pseudo + " <-> " + data);
		}
	});

	socket.on('askConn', function (data) {
		var pseudo = getMyPseudo(this);
		if(pair[pseudo][0]) {
			var pseudo = getMyPseudo(this);
			
			if(data == 'A') {// Allow
				pair[pseudo][2].emit("connTo", "A");
				logIO("[!] " + pseudo + " <-> " + getMyPseudo(pair[pseudo][2]));
				connection.query('CALL `startGame`();');
			} else { // Deny
				pair[pseudo][2].emit("connTo", "R");
				logIO("[N] " + pseudo + " <-> " + getMyPseudo(pair[pseudo][2]));

				par[pseudo][0] = false;
				par[pseudo][2] = 0;

				par[data][0] = false;
				par[data][2] = 0;
			}
		}
	});


	socket.on('send', function (data) {
		pair[getMyPseudo(this)][2].emit("send", data);
	});
    
    socket.on('rtc', function (data) {
		pair[getMyPseudo(this)][2].emit("rtc", data);
	});
});

// Cleanup when app is closing
process.on('exit', function(code) {
	console.log("\n\n");
	logPENDING("Exiting");
	connection.end();
	logOK("Exiting :" + code);
});

process.on('SIGINT', process.exit);
process.on('uncaughtException', process.exit);


var startTime = new Date();

logOK("Loading");
